// Описать своими словами в несколько строчек, зачем в программировании нужны циклы.

//Циклы в программировании используются при необходимости повторять действие, пока не будет достигнуто определенное (заданное) условие.

let userNumber = prompt("Введите число");
while (userNumber % 1 > 0) {
  userNumber = prompt("Введите число");
}

if (userNumber < 5) {
  console.log("Sorry, no numbers");
} else
  for (i = 1; i <= userNumber; i++) {
    result = i % 5;
    if (result === 0) {
      console.log(i);
    }
  }
